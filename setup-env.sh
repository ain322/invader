#!/bin/sh

# Homebrew
which brew > /dev/null 2>&1
if [ $? != 0 ]
then
	printf "\e[33mHomebrew:\e[0m not found, installing\n"
	ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
	printf "\e[33mHomebrew:\e[0m found\n"
fi

brew update
brew upgrade


# Cask
brew-cask > /dev/null 2>&1
if [ $? != 0 ]
then
	printf "\e[33mCask:\e[0m not found, installing\n"
	brew install caskroom/cask/brew-cask
else
	printf "\e[33mCask:\e[0m found\n"
fi

brew-cask update


# Git and extras
which brew > /dev/null 2>&1
if [ $? != 0 ]
then
	printf "\e[33mgit:\e[0m not found, installing\n"
	brew install git
else
	printf "\e[33mgit:\e[0m found\n"
fi

brew leaves | grep "git-extras" >> /dev/null 2>&1
if [ $? != 0 ]
then
	printf "\e[33mgit-extras:\e[0m not found, installing\n"
	brew install git-extras
else
	printf "\e[33mgit-extras:\e[0m found\n"
fi

# zsh and oh my
brew leaves | grep "zsh" >> /dev/null 2>&1
if [ $? != 0 ]
then
	printf "\e[33mzsh:\e[0m not found, installing\n"
	brew install zsh
else
	printf "\e[33mzsh:\e[0m found\n"
fi

sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"


brew cask install iterm2
brew cask install google-chrome
brew cask install skype
brew cask install dockertoolbox
brew cask install vagrant

sed -i bak "s|plugins=(.*)|plugins=(git git-extras brew brew-cask docker grunt jump npm nvm vagrant node)|" ~/.zshrc

brew install nvm

mkdir -p ~/.nvm

#Remove generated block
sed -i bak -n '1h;1!H;${;g;s|##### GENERATED START #####.*##### GENERATED END #####||g;p;}' ~/.zshrc

echo "##### GENERATED START #####" >> ~/.zshrc
echo "export NVM_DIR=~/.nvm" >> ~/.zshrc
echo "source $(brew --prefix nvm)/nvm.sh" >> ~/.zshrc

cat .zshrc_vagrant_template >> ~/.zshrc
cat .zshrc_docker_template >> ~/.zshrc

echo "##### GENERATED END #####" >> ~/.zshrc

export NVM_DIR=~/.nvm
source $(brew --prefix nvm)/nvm.sh
nvm install 4.1.2
